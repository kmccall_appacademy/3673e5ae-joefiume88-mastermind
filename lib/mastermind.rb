class Code
  attr_reader :pegs

  PEGS = {
    "B" => :blue,
    "G" => :green,
    "O" => :orange,
    "P" => :purple,
    "R" => :red,
    "Y" => :yellow
    }

  def initialize(pegs)
    @pegs = pegs
  end

  def self.parse(str)
    pegs = str.split("").map do |letter|
      raise "error" unless PEGS.has_key?(letter.upcase)

      PEGS[letter.upcase]
    end

    Code.new(pegs)
  end

  def self.random
    random_code = Code.new(PEGS.values.sample(4))
  end

  def [](idx)
    pegs[idx]
  end

  def exact_matches(other_code)
    exact_matches_count = 0
    pegs.each_index do |idx|
      exact_matches_count += 1 if self[idx] == other_code[idx]
    end

    exact_matches_count
  end

  def near_matches(other_code)
    other_color_counts = other_code.color_counts

    near_matches_count = 0
    self.color_counts.each do |color, count|
      next unless other_color_counts.has_key?(color)

      near_matches_count += [count, other_color_counts[color]].min
    end

    near_matches_count - self.exact_matches(other_code)
  end

  def ==(other_code)
    return false unless other_code.is_a?(Code)

    self.pegs == other_code.pegs
  end

  protected

  def color_counts
    color_counts = Hash.new(0)

    @pegs.each do |color|
      color_counts[color] += 1
    end

    color_counts
  end
end

class Game
  attr_reader :secret_code

  MAX_TURNS = 10

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  def play
    MAX_TURNS.times do
      guess = get_guess

      if guess == @secret_code
        puts "You won!"
        return
      end

      display_matches(guess)
    end

    puts "Sorry, you lost. The correct code was #{secret_code}"
  end

  def get_guess
    puts "please enter your guess:"

    begin
      Code.parse(gets.chomp)
    rescue
      puts "Error!"
      retry
    end
  end

  def display_matches(guess)
    exact_matches_count = @secret_code.exact_matches(guess)
    near_matches_count = @secret_code.near_matches(guess)

    puts "You got #{exact_matches_count} exact matches!"
    puts "You got #{near_matches_count} near matches!"
  end
end

if __FILE__ == $PROGRAM_NAME
  Game.new.play
end
